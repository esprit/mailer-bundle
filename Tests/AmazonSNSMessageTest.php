<?php

namespace MailerBundle\Tests\Services\Mail;

use MailerBundle\Entity\Embedded\AmazonSNSStatus;
use MailerBundle\AmazonSNSMessage;
use MailerBundle\EmailStatusUpdate;
use MailerBundle\Exceptions\AmazonSNSMessageException;
use PHPUnit\Framework\TestCase;

class AmazonSNSMessageTest extends TestCase
{
    public function getValidData()
    {
        $success = <<<REQUEST
{
    "Type": "Notification",
    "MessageId": "0e46871a-4659-5274-9d33-d564da8ae4d9",
    "TopicArn": "arn:aws:sns:us-west-2:376067477311:report",
    "Message": "{\"notificationType\":\"Delivery\",\"mail\":{\"timestamp\":\"2017-11-09T18:12:34.348Z\",\"source\":\"spin.test1@gmail.com\",\"sourceArn\":\"arn:aws:ses:us-west-2:376067477311:identity\/spin.test1@gmail.com\",\"sourceIp\":\"188.237.126.103\",\"sendingAccountId\":\"376067477311\",\"messageId\":\"0101015fa1fbcbac-2aa80da7-0c2a-40bc-81ee-9dcc608cc547-000000\",\"destination\":[\"success@simulator.amazonses.com\"]},\"delivery\":{\"timestamp\":\"2017-11-09T18:12:35.472Z\",\"processingTimeMillis\":1124,\"recipients\":[\"success@simulator.amazonses.com\"],\"smtpResponse\":\"250 2.6.0 Message received\",\"remoteMtaIp\":\"205.251.242.49\",\"reportingMTA\":\"a27-23.smtp-out.us-west-2.amazonses.com\"}}",
    "Timestamp": "2017-11-09T18:12:35.539Z",
    "SignatureVersion": "1",
    "Signature": "NI706xB5Ms6i9NcsWgIfgb74rCZIqZxM4GvMxwAkq5M0TXLd9x1z+3WbOcDenHWXNUmaS+85iDwqqTM0g8DwjqupwOdMRoXyE97va5iNrEyBaJpCz6Mca1NxuUWl+qFATdylM2uPV1CgHBGhtm\/0ISlBX4HRff\/1FgeBnkUKvlE4iVTKa1LDMkjSPT24ZCkxSarW8ePNQ1HDF2mCLj+RHN+3g0EwbNW4jexldB6H9kPzrnXDwDjC14nqsW5c2s2fNo0x\/n8LxDNMPl+nKuKkjl1LuwVre4WHNslL823xJpde5oaoDNfdA+\/bVECBrZFy6smC+fsu7J8lzqX1eEhbew==",
    "SigningCertURL": "https:\/\/sns.us-west-2.amazonaws.com\/SimpleNotificationService-433026a4050d206028891664da859041.pem",
    "UnsubscribeURL": "https:\/\/sns.us-west-2.amazonaws.com\/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:376067477311:report:357bb167-c7d1-4837-a66f-cf6df6c1771b"
}  
REQUEST;

        $bounce = <<<REQUEST
{
    "Type": "Notification",
    "MessageId": "d67ee30d-3aa7-5bbf-b112-b17393eb6867",
    "TopicArn": "arn:aws:sns:us-west-2:376067477311:report",
    "Message": "{\"notificationType\":\"Bounce\",\"bounce\":{\"bounceType\":\"Permanent\",\"bounceSubType\":\"General\",\"bouncedRecipients\":[{\"emailAddress\":\"bounce@simulator.amazonses.com\",\"action\":\"failed\",\"status\":\"5.1.1\",\"diagnosticCode\":\"smtp; 550 5.1.1 user unknown\"}],\"timestamp\":\"2017-11-09T19:05:48.349Z\",\"feedbackId\":\"0101015fa22c87e3-447a45e7-42f5-4492-b2c2-73efb06a5cf2-000000\",\"remoteMtaIp\":\"205.251.242.49\",\"reportingMTA\":\"dsn; a27-45.smtp-out.us-west-2.amazonses.com\"},\"mail\":{\"timestamp\":\"2017-11-09T19:05:46.000Z\",\"source\":\"spin.test1@gmail.com\",\"sourceArn\":\"arn:aws:ses:us-west-2:376067477311:identity\/spin.test1@gmail.com\",\"sourceIp\":\"188.237.126.103\",\"sendingAccountId\":\"376067477311\",\"messageId\":\"0101015fa22c81eb-7bd655a5-47f7-4e43-9ca8-4df26e97fe47-000000\",\"destination\":[\"bounce@simulator.amazonses.com\"]}}",
    "Timestamp": "2017-11-09T19:05:48.394Z",
    "SignatureVersion": "1",
    "Signature": "KwOaKD2gXAk5VTdm\/QMQbAkl1CWOYxhTZLvjc8Uh3IavGrrOUXTEyo9bfdJ\/lvlguNfiTUf7HS8VXbcM4TpbgPg2JqIPYU28XBHuo9x9Imml40iGxwK7LT6iM7LHUjfKfWf9wJ1tqpV9tNEDT90AsYnpgzoLbbuLpGNtykfJzcjAs+mw9YQL2T+vIg3KpsQrlWA7iPrK8HhxyXhTAx9BMJOXB6A5yroUhHUib6gmzDARyZc28NLmihXKxjUJfJlFsHIyAbEsZOZeaVz+AiSZQK1ekmyMxJQIDtz92vKlib3auDHExR1qRNdVSeNmYMaOwTnWpphaxGae3kLQPXZV0Q==",
    "SigningCertURL": "https:\/\/sns.us-west-2.amazonaws.com\/SimpleNotificationService-433026a4050d206028891664da859041.pem",
    "UnsubscribeURL": "https:\/\/sns.us-west-2.amazonaws.com\/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:376067477311:report:357bb167-c7d1-4837-a66f-cf6df6c1771b"
} 
REQUEST;

        $complaint = <<<REQUEST
{
    "Type": "Notification",
    "MessageId": "7bbd23a6-336e-5bab-aa0a-71d89861e1ee",
    "TopicArn": "arn:aws:sns:us-west-2:376067477311:report",
    "Message": "{\"notificationType\":\"Complaint\",\"complaint\":{\"complainedRecipients\":[{\"emailAddress\":\"complaint@simulator.amazonses.com\"}],\"timestamp\":\"2017-11-09T19:13:18.000Z\",\"feedbackId\":\"0101015fa23369ac-0bcc7af6-c582-11e7-a16b-e9dfdb03c37f-000000\",\"userAgent\":\"Amazon SES Mailbox Simulator\",\"complaintFeedbackType\":\"abuse\"},\"mail\":{\"timestamp\":\"2017-11-09T19:13:19.357Z\",\"source\":\"spin.test1@gmail.com\",\"sourceArn\":\"arn:aws:ses:us-west-2:376067477311:identity\/spin.test1@gmail.com\",\"sourceIp\":\"188.237.126.103\",\"sendingAccountId\":\"376067477311\",\"messageId\":\"0101015fa233604e-0e2a9cc8-afe3-46df-966b-1395465368b3-000000\",\"destination\":[\"complaint@simulator.amazonses.com\"]}}",
    "Timestamp": "2017-11-09T19:13:19.389Z",
    "SignatureVersion": "1",
    "Signature": "cOMGNApKu99DnrjFg3mXRrP8rxvfuE0im+7Qs+f2OWtr6M0IIK+k4g6HyMX3sHH76FTXn5UQB7eYtBfB9zIva+4BrVpZpBiCg3VZDeHpscRlqbkD\/tSIVoA+A2f+k+FVZL9caDmghODLAJdmr6BZTRDjnfXkTscIRVpQYaeks61Sfnk\/8PcN5Vm8eo9HyOfcfHZXg0KeavIXUoFdDwCbJsPRpEPtbx2zKPiHbWxiaB7leGV4zlgPUfmwVJ1YBKslygTYvB4ro0jcc5+gZ4CpuxKMbxEdK3zm0kGIAbmfPJqvBBAPr5oeRKv3A1oJzkJcap+yZyK8iLeeSQ\/KcHaO0A==",
    "SigningCertURL": "https:\/\/sns.us-west-2.amazonaws.com\/SimpleNotificationService-433026a4050d206028891664da859041.pem",
    "UnsubscribeURL": "https:\/\/sns.us-west-2.amazonaws.com\/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:376067477311:report:357bb167-c7d1-4837-a66f-cf6df6c1771b"
} 
REQUEST;

        return [
            [$success, 'Delivery', false, 'success@simulator.amazonses.com', '0101015fa1fbcbac-2aa80da7-0c2a-40bc-81ee-9dcc608cc547-000000'],
            [$bounce, 'Bounce', true, 'bounce@simulator.amazonses.com', '0101015fa22c81eb-7bd655a5-47f7-4e43-9ca8-4df26e97fe47-000000'],
            [$complaint, 'Complaint', true, 'complaint@simulator.amazonses.com', '0101015fa233604e-0e2a9cc8-afe3-46df-966b-1395465368b3-000000'],
        ];
    }

    /**
     * @dataProvider getValidData
     * @covers AmazonSNSMessage::fromRequest()
     * @covers AmazonSNSMessage::getUpdates()
     */
    public function test_fromRequest($request, $status, $rejected, $email, $messageId)
    {
        $message = AmazonSNSMessage::fromRequest($request);
        $this->assertInstanceOf(AmazonSNSMessage::class, $message);

        $updates = $message->getUpdates();
        $this->assertEquals(1, count($updates));

        $update = $message->getUpdates()[0];
        $this->assertInstanceOf(EmailStatusUpdate::class, $update);
        $this->assertInstanceOf(AmazonSNSStatus::class, $update->getStatus());

        $this->assertEquals($email, $update->getEmail());
        $this->assertEquals($messageId, $update->getMessageId());
        $this->assertEquals($status, $update->getStatus()->getType());
        $this->assertEquals($rejected, $update->getStatus()->isRejected());
    }

    public function getInvalidData()
    {
        $subscription = <<<REQUEST
{
    "Type": "SubscriptionConfirmation",
    "MessageId": "2eee99d4-d715-490f-87dd-1066102e30a6",
    "Token": "2336412f37fb687f5d51e6e241d59b69a62a729402f8024b6532f9f1a9539b7706eaec7d8fcb408e6954bcaad01513e54995f7c2109fe01df9b56ca057ea025653051421bf94b54d4fc84028f6917b902349cbed7f073e2652dd0813decf278432455fcebb9739f8b20d3a354793baba",
    "TopicArn": "arn:aws:sns:us-west-2:376067477311:report",
    "Message": "You have chosen to subscribe to the topic arn:aws:sns:us-west-2:376067477311:report.\nTo confirm the subscription, visit the SubscribeURL included in this message.",
    "SubscribeURL": "https:\/\/sns.us-west-2.amazonaws.com\/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-west-2:376067477311:report&Token=2336412f37fb687f5d51e6e241d59b69a62a729402f8024b6532f9f1a9539b7706eaec7d8fcb408e6954bcaad01513e54995f7c2109fe01df9b56ca057ea025653051421bf94b54d4fc84028f6917b902349cbed7f073e2652dd0813decf278432455fcebb9739f8b20d3a354793baba",
    "Timestamp": "2017-11-09T14:55:28.747Z",
    "SignatureVersion": "1",
    "Signature": "YDophwYzDnvDJNGl2tgGk8MVMLuacDlBdxbTdJUnX6QqvPFvfRT0AOr69mqgfR9lp0yBOH0C1psMQrXqSN3MKHyYAPCZ9UJUIZTmnUQfHZWSG3+yyBxaeJCH\/\/KXipz\/NhL4zrnfTG9bVcE4ox+tIVJjawKd8EPE\/1FlxWzUo\/OzNtxd3mfCB3FkKimt3UFhdNF\/V9IQlXB7gYe2ZXKg7mC6FahQr7\/C1uHITzN0wiOH2INtE1SI+g84MYAA2+zlvMrTicMNRJDZXgt6555PWK+YoywvJDekbBnDvt28n2Hk8AxjMyt+Za8xi32MYVsMeo1N98UmBnF5JMicXtUlYA==",
    "SigningCertURL": "https:\/\/sns.us-west-2.amazonaws.com\/SimpleNotificationService-433026a4050d206028891664da859041.pem"
}  
REQUEST;

        return [
            [$subscription],
        ];
    }

    /**
     * @dataProvider getInvalidData
     * @covers AmazonSNSMessage::fromRequest()
     */
    public function test_fromRequestInvalid($request)
    {
        $this->expectException(AmazonSNSMessageException::class);
        AmazonSNSMessage::fromRequest($request);
    }
}