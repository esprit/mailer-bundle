<?php


namespace MailerBundle\Tests\Entity;


use MailerBundle\Entity\EmailQueue;
use MailerBundle\Entity\Embedded\AmazonSESResult;
use MailerBundle\Entity\Embedded\EmailMessage;
use MailerBundle\Entity\Embedded\AmazonSNSStatus;
use PHPUnit\Framework\TestCase;

class EmailQueueTest extends TestCase
{
    public function createTestEmail()
    {
        $email = new EmailQueue(
            (new EmailMessage('test', 'test', 'teat'))
                ->setRoute('from@mail.con', 'to@mail.com')
        );

        return [[$email]];
    }

    /**
     * @dataProvider createTestEmail
     * @covers EmailQueue::isSent()
     */
    public function test_isSent(EmailQueue $email)
    {
        $this->assertEquals(false, $email->isSent());

        $email->sendSuccess(new AmazonSESResult('test-messageId'));
        $this->assertEquals(true, $email->isSent());
    }

    /**
     * @dataProvider createTestEmail
     * @covers EmailQueue::isRejected()
     */
    public function test_isRejected(EmailQueue $email)
    {
        $this->assertEquals(false, $email->isRejected());

        $email->setStatus(new AmazonSNSStatus(AmazonSNSStatus::BOUNCE, 'Some reason'));
        $this->assertEquals(true, $email->isRejected());

        $email->setStatus(new AmazonSNSStatus(AmazonSNSStatus::DELIVERY));
        $this->assertEquals(false, $email->isRejected());

        $email->setStatus(new AmazonSNSStatus(AmazonSNSStatus::COMPLAINT));
        $this->assertEquals(true, $email->isRejected());
    }
}