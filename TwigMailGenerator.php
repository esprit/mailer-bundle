<?php

namespace MailerBundle;

use MailerBundle\Entity\Embedded\EmailMessage;
use Twig\Environment;

class TwigMailGenerator
{
    /** @var Environment */
    private $twig;

    /** @var array */
    private $config;

    /**
     * TwigMailGenerator constructor.
     * @param Environment $twig
     * @param array $config
     */
    public function __construct(Environment $twig, array $config = [])
    {
        $this->twig = $twig;
        $this->config = $config;
    }

    private function getConfig($name, $default)
    {
        return $this->config[$name] ?? $default;
    }

    /**
     * @param string $template
     * @param array $parameters
     * @return EmailMessage
     */
    public function getMessage(string $template, array $parameters = [])
    {
        $template = $this->twig->load($template);

        $subject  = $template->renderBlock($this->getConfig('subject_block', 'subject'),   $parameters);
        $bodyHtml = $template->renderBlock($this->getConfig('body_html_block', 'body_html'), $parameters);
        $bodyText = $template->renderBlock($this->getConfig('body_text_block', 'body_text'), $parameters);

        return new EmailMessage($subject, $bodyHtml, $bodyText);
    }
}