<?php

namespace MailerBundle;

use MailerBundle\Entity\EmailQueue;
use MailerBundle\Entity\Embedded\AmazonSESResult;
use MailerBundle\Entity\Embedded\EmailError;
use MailerBundle\Entity\Embedded\EmailMessage;
use Aws\Result;
use Aws\Sdk;
use Aws\Ses\Exception\SesException;
use Aws\Ses\SesClient;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;

class AmazonSESMailSender implements MailSenderInterface
{
    /** @var SesClient */
    private $client;

    /** @var Logger */
    private $logger;

    /**
     * AmazonSESMailSender constructor.
     * @param Sdk $sdk
     * @param EntityManager $em
     * @param Logger $logger
     * @internal param array $config
     */
    public function __construct(Sdk $sdk, Logger $logger)
    {
        $this->client = $sdk->createSes();
        $this->logger = $logger;
    }

    public function send(EmailMessage $message): AmazonSESResult
    {
        return $this->processSESSend($message);
    }

    private function createSESRequest(EmailMessage $message) : array
    {
        $body = [];
        if ($message->getBodyText()) {
            $body['Text'] = [
                'Data' => $message->getBodyText(),
                'Charset' => 'UTF-8',
            ];
        }

        if ($message->getBodyHtml()) {
            $body['Html'] = [
                'Data' => $message->getBodyHtml(),
                'Charset' => 'UTF-8',
            ];
        }

        return [
            'Source' => $message->getFrom(),
            'Destination' => [
                'ToAddresses' => [$message->getTo()],
            ],
            'Message' => [
                'Subject' => [
                    'Data' => $message->getSubject(),
                    'Charset' => 'UTF-8',
                ],
                'Body' => $body,
            ],
            'ReplyToAddresses' => [$message->getFrom()],
            'ReturnPath' => $message->getFrom(),
        ];
    }

    private function processSESSend(EmailMessage $message) : AmazonSESResult
    {
        $request = $this->createSESRequest($message);

        $this->logger->debug('Amazon SES sender request', $request);

        // http://docs.aws.amazon.com/aws-sdk-php/v2/api/class-Aws.Ses.SesClient.html#_sendEmail
        /** @var Result $response */
        $response = $this->client->sendEmail($request);

        $this->logger->debug('Amazon SES sender response', $response->toArray());

        return new AmazonSESResult($response['MessageId']);
    }
}