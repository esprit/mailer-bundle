<?php


namespace MailerBundle;

use Symfony\Component\Messenger\MessageBusInterface;

class Mailer implements MailerInterface
{
    /** @var MessageBusInterface */
    private $messageBus;

    /** @var TwigMailGenerator */
    private $generator;

    /** @var array */
    private $config;

    public function __construct(MessageBusInterface $messageBus, TwigMailGenerator $generator, array $config)
    {
        $this->messageBus = $messageBus;
        $this->generator = $generator;
        $this->config = $config;
    }

    public function send(string $email, string $template, array $parameters = [])
    {
        $message  = $this->generator->getMessage($template, $parameters);
        $message->setRoute($this->config['from'], $email);

        $this->messageBus->dispatch($message);
    }
}