<?php

namespace MailerBundle;

use Doctrine\ORM\EntityManagerInterface;
use MailerBundle\Entity\EmailQueue;
use MailerBundle\Entity\Embedded\EmailError;
use MailerBundle\Entity\Embedded\EmailMessage;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageSubscriberInterface;

class EmailMessageSender implements MessageSubscriberInterface
{
    /** @var MailSenderInterface */
    private $sender;

    private $em;

    private $blacklist;

    private $logger;

    public function __construct(EntityManagerInterface $em, Blacklist $blacklist, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->blacklist = $blacklist;
        $this->logger = $logger;
    }

    public function send(EmailMessage $message)
    {
        $email = new EmailQueue($message);
        $this->em->persist($email);
        // looks like "pending" status is not useful

        if ($this->blacklist->hasEmail($email->getToAdderss())) {

            $email->markAsBlacklisted();
            $this->logger->debug('Email in blacklist', ['email' => $email->getToAdderss()]);

            $this->em->flush();
            return;
        }

        try {
            $result = $this->sender->send($message);

            $this->logger->debug('Email successfully sent', [
                'email' => $email->getId(),
                'result' => (string) $result,
                'transport' => get_class($this->sender),
            ]);

            $email->sendSuccess($result);
            $this->em->flush();

        } catch (\Exception $e) {
            $this->logger->error('Email sent error', [
                'email' => $email->getId(),
                'error' => $e->getMessage(),
                'transport' => get_class($this->sender),
            ]);

            // save exception message in database
            $email->sendError(new EmailError($e->getCode(), $e->getMessage()));
            $this->em->flush();

            throw $e;
        }
    }

    /**
     * @inheritDoc
     */
    public static function getHandledMessages(): iterable
    {
        yield EmailMessage::class => ['method' => 'send'];
    }

    /**
     * @param MailSenderInterface $sender
     */
    public function setTransport(MailSenderInterface $sender): void
    {
        $this->sender = $sender;
    }
}