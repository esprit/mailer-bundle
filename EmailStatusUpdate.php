<?php


namespace MailerBundle;


use MailerBundle\Entity\Embedded\AmazonSNSStatus;

class EmailStatusUpdate
{
    /** @var string */
    private $email;

    /** @var string */
    private $messageId;

    /** @var AmazonSNSStatus */
    private $status;

    /**
     * EmailStatusUpdates constructor.
     * @param string $email
     * @param string $messageId
     * @param AmazonSNSStatus $status
     */
    public function __construct(string $email, string $messageId, AmazonSNSStatus $status)
    {
        $this->email = $email;
        $this->messageId = $messageId;
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getMessageId(): string
    {
        return $this->messageId;
    }

    /**
     * @return AmazonSNSStatus
     */
    public function getStatus(): AmazonSNSStatus
    {
        return $this->status;
    }
}