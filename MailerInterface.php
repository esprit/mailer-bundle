<?php

namespace MailerBundle;

interface MailerInterface
{
    public function send(string $email, string $template, array $parameters = []);
}