<?php

namespace MailerBundle;

use MailerBundle\Entity\EmailQueue;
use MailerBundle\Entity\Embedded\AmazonSESResult;
use MailerBundle\Entity\Embedded\EmailError;
use MailerBundle\Entity\Embedded\EmailMessage;
use Aws\Result;
use Aws\Sdk;
use Aws\Ses\Exception\SesException;
use Aws\Ses\SesClient;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;

class NullSender implements MailSenderInterface
{
    public function send(EmailMessage $message): AmazonSESResult
    {
        return new AmazonSESResult();
    }
}