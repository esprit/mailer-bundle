<?php


namespace MailerBundle;


interface AmazomSNSStatusInterface
{
    const DELIVERY = 'Delivery';
    const BOUNCE = 'Bounce';
    const COMPLAINT = 'Complaint';
}