<?php

namespace MailerBundle;

use MailerBundle\Entity\Embedded\AmazonSESResult;
use MailerBundle\Entity\Embedded\EmailMessage;

interface MailSenderInterface
{
    public function send(EmailMessage $message): AmazonSESResult;
}