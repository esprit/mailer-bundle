<?php

namespace MailerBundle;

use MailerBundle\Entity\BlacklistedEmail;
use MailerBundle\Repository\BlacklistedEmailRepository;
use Symfony\Bridge\Doctrine\ManagerRegistry;

class Blacklist
{
    private $registry;

    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
    }

    private function getRepository() : BlacklistedEmailRepository
    {
        return $this->registry->getRepository(BlacklistedEmail::class);
    }

    public function hasEmail(string $email) : bool
    {
        return $this->getEmail($email) ? true : false;
    }

    public function getEmail(string $email): ?BlacklistedEmail
    {
        return $this->getRepository()->findOneBy(['email' => $email]);
    }

    public function addEmail(BlacklistedEmail $blacklistedEmail)
    {
        if ($this->hasEmail($blacklistedEmail->getEmail())) {
            return;
        }

        $this->registry->getManager()->persist($blacklistedEmail);
    }
}