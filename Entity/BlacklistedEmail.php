<?php

namespace MailerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MailerBundle\Repository\BlacklistedEmailRepository")
 * @ORM\Table(name="blacklisted_email")
 */
class BlacklistedEmail
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $email;

    /**
     * @var EmailQueue
     * @ORM\ManyToOne(targetEntity="EmailQueue")
     */
    private $reason;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * BlacklistedEmail constructor.
     * @param $email
     * @param $reason
     */
    public function __construct(EmailQueue $reason)
    {
        $this->email = $reason->getMessage()->to;
        $this->reason = $reason;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}