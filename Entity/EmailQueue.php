<?php

namespace MailerBundle\Entity;

use MailerBundle\Entity\Embedded\AmazonSESResult;
use MailerBundle\Entity\Embedded\EmailError;
use MailerBundle\Entity\Embedded\EmailMessage;
use MailerBundle\Entity\Embedded\AmazonSNSStatus;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MailerBundle\Repository\EmailQueueRepository")
 * @ORM\Table(name="email_queue")
 */
class EmailQueue
{
    const DELIVERY_PENDING = 'pending';          // Ожидает отправки
    const DELIVERY_SUCCESS = 'success';          // Успешно отправлено
    const DELIVERY_BLACKLISTED = 'blacklisted';  // Во время отправки было обнаружено, что емейл в черном списке
    const DELIVERY_ERROR = 'error';              // Во время отправки произошла ошибка

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var EmailMessage
     * @ORM\Embedded(class="MailerBundle\Entity\Embedded\EmailMessage")
     */
    private $message;

    /**
     * @var AmazonSESResult
     * @ORM\Embedded(class="MailerBundle\Entity\Embedded\AmazonSESResult")
     */
    private $result;

    /**
     * @var AmazonSNSStatus
     * @ORM\Embedded(class="MailerBundle\Entity\Embedded\AmazonSNSStatus")
     */
    private $status;

    /**
     * @var EmailError
     * @ORM\Embedded(class="MailerBundle\Entity\Embedded\EmailError")
     */
    private $error;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $delivery;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * EmailQueue constructor.
     * @param EmailMessage $message
     */
    public function __construct(EmailMessage $message)
    {
        $this->message = $message;
        $this->delivery = self::DELIVERY_PENDING;
        $this->createdAt = new \DateTime();
    }

    /**
     * @param AmazonSESResult $result
     */
    public function sendSuccess(AmazonSESResult $result)
    {
        $this->result = $result;
        $this->delivery = self::DELIVERY_SUCCESS;
    }

    /**
     * @param AmazonSNSStatus $status
     */
    public function setStatus(AmazonSNSStatus $status)
    {
        $this->status = $status;
    }

    /**
     * @param EmailError $error
     */
    public function sendError(EmailError $error)
    {
        $this->error = $error;
        $this->delivery = self::DELIVERY_ERROR;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return EmailMessage
     */
    public function getMessage(): EmailMessage
    {
        return $this->message;
    }

    /**
     * @return AmazonSESResult
     */
    public function getResult(): AmazonSESResult
    {
        return $this->result;
    }

    /**
     * @return AmazonSNSStatus
     */
    public function getStatus(): AmazonSNSStatus
    {
        return $this->status;
    }

    public function isSent() : bool
    {
        return $this->delivery === self::DELIVERY_SUCCESS;
    }

    public function isRejected() : bool
    {
        if (! $this->status) {
            return false;
        }

        return $this->status->isRejected();
    }

    public function markAsBlacklisted()
    {
        $this->delivery = self::DELIVERY_BLACKLISTED;
    }

    public function getToAdderss() : string
    {
        return $this->message->to;
    }
}