<?php

namespace MailerBundle\Entity\Embedded;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Embeddable() */
class AmazonSESResult
{
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $messageId;

    /**
     * AmazonSESResponse constructor.
     * @param string $messageId
     * @param string $metadata
     */
    public function __construct(string $messageId = null)
    {
        $this->messageId = $messageId;
    }

    public function __toString()
    {
        return (string) $this->messageId;
    }
}