<?php


namespace MailerBundle\Entity\Embedded;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
class EmailMessage
{
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    public $from;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    public $to;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    public $subject;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    public $bodyHtml;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    public $bodyText;

    /**
     * EmailMessage constructor.
     * @param $from
     * @param $to
     * @param $subject
     * @param $bodyHtml
     * @param $bodyText
     */
    public function __construct(string $subject, string $bodyHtml, string $bodyText)
    {
        $this->subject = $subject;
        $this->bodyHtml = $bodyHtml;
        $this->bodyText = $bodyText;
    }

    public function setRoute(string $from, string $to)
    {
        $this->from = $from;
        $this->to = $to;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return mixed
     */
    public function getBodyHtml()
    {
        return $this->bodyHtml;
    }

    /**
     * @return mixed
     */
    public function getBodyText()
    {
        return $this->bodyText;
    }
}