<?php

namespace MailerBundle\Entity\Embedded;

use MailerBundle\AmazomSNSStatusInterface;
use Doctrine\ORM\Mapping as ORM;

/** @ORM\Embeddable() */
class AmazonSNSStatus implements AmazomSNSStatusInterface
{
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $message;

    public function __toString()
    {
        return $this->type;
    }

    public function __construct(string $type, $message = null)
    {
        $this->type = $type;
        $this->message = $message;
    }

    public function isRejected() : bool
    {
        return $this->type === self::BOUNCE or $this->type === self::COMPLAINT;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}