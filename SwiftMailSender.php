<?php

namespace MailerBundle;

use MailerBundle\Entity\Embedded\AmazonSESResult;
use MailerBundle\Entity\Embedded\EmailMessage;

class SwiftMailSender implements MailSenderInterface
{
    /** @var \Swift_Mailer */
    private $mailer;

    /**
     * SwiftMailSender constructor.
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send(EmailMessage $message): AmazonSESResult
    {
        $m = new \Swift_Message($message->getSubject());
        if ($message->getBodyText()) {
            $m->setBody($message->getBodyText(), 'text/plain');
        }
        if ($message->getBodyHtml()) {
            $m->addPart($message->getBodyHtml(), 'text/html');
        }
        $m->setFrom($message->getFrom());
        $m->setTo($message->getTo());

        $this->mailer->send($m);

        return new AmazonSESResult();
    }
}