<?php

namespace MailerBundle;

use MailerBundle\Entity\Embedded\AmazonSESResult;
use MailerBundle\Entity\Embedded\EmailMessage;
use Symfony\Component\Mime\Email;

class SymfonyMailerSender implements MailSenderInterface
{
    private $mailer;

    public function __construct(\Symfony\Component\Mailer\MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send(EmailMessage $message): AmazonSESResult
    {
        $email = (new Email())
            ->from($message->getFrom())
            ->to($message->getTo())
            ->subject($message->getSubject());

        if ($message->getBodyText()) {
            $email->text($message->getBodyText());
        }
        if ($message->getBodyHtml()) {
            $email->html($message->getBodyHtml());
        }

        $this->mailer->send($email);

        return new AmazonSESResult();
    }
}