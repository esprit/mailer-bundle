<?php


namespace MailerBundle;


use MailerBundle\Entity\Embedded\AmazonSNSStatus;
use MailerBundle\Exceptions\AmazonSNSMessageException;

class AmazonSNSMessage implements AmazomSNSStatusInterface
{
    /** @var array */
    private $data;

    /**
     * @param string $request
     * @return AmazonSNSMessage
     * @throws AmazonSNSMessageException
     */
    public static function fromRequest(string $request)
    {
        $requestData = json_decode($request, true);

        if ($requestData['Type'] !== "Notification") {
            throw new AmazonSNSMessageException("Unexpected notification type in message in: " . $request);
        }

        return new self(json_decode($requestData['Message'], true));
    }

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    private function getType() : string
    {
        return $this->data['notificationType'];
    }

    private function isType(string $type) : bool
    {
        return $this->getType() === $type;
    }

    private function getMessageId() : string
    {
        return $this->data['mail']['messageId'];
    }

    /**
     * @return EmailStatusUpdate[]
     * @throws \Exception
     */
    public function getUpdates() : array
    {
        $updates = [];

        if ($this->isType(self::DELIVERY)) {

            foreach ($this->data['delivery']['recipients'] as $recipient) {
                $updates[] = new EmailStatusUpdate(
                    $recipient,
                    $this->getMessageId(),
                    new AmazonSNSStatus($this->getType())
                );
            }

            return $updates;
        }

        if ($this->isType(self::BOUNCE)) {

            foreach ($this->data['bounce']['bouncedRecipients'] as $recipient) {
                $updates[] = new EmailStatusUpdate(
                    $recipient['emailAddress'],
                    $this->getMessageId(),
                    new AmazonSNSStatus($this->getType(), $recipient['diagnosticCode'])
                );
            }

            return $updates;
        }

        if ($this->isType(self::COMPLAINT)) {

            foreach ($this->data['complaint']['complainedRecipients'] as $recipient) {
                $updates[] = new EmailStatusUpdate(
                    $recipient['emailAddress'],
                    $this->getMessageId(),
                    new AmazonSNSStatus($this->getType())
                );
            }

            return $updates;
        }

        throw new \Exception('Cant create updates from notification type ' .  $this->getType());
    }
}