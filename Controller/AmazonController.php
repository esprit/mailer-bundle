<?php

namespace MailerBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use MailerBundle\Entity\BlacklistedEmail;
use MailerBundle\Repository\EmailQueueRepository;
use MailerBundle\AmazonSNSMessage;
use MailerBundle\Blacklist;
use MailerBundle\Exceptions\AmazonSNSMessageException;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * NOTE: для валидации урла надо скопировать урл из поля SubscribeURL в амазоновскую форму SNS)
 * @see https://docs.aws.amazon.com/ses/latest/DeveloperGuide/notification-examples.html
 */
class AmazonController extends AbstractController
{
    /**
     * @Route("/i/amazon/ses-callback")
     */
    public function sesCallbackAction(Request $request, EntityManagerInterface $em)
    {
        $emails = $em->getRepository('MailerBundle:EmailQueue');
        $logger = $this->get('monolog.logger.mailer');
        $blacklist = $this->get('mailer.blacklist');
        /** @var EmailQueueRepository $emails */
        /** @var Logger $logger */
        /** @var Blacklist $blacklist */

        try {

            $content = $request->getContent();
            $logger->debug('Aws SNS raw request content: ' . $content);

            if (! $content) {
                throw new AmazonSNSMessageException('Empty content');
            }

            $message = AmazonSNSMessage::fromRequest($content);
            foreach ($message->getUpdates() as $update) {

                $email = $emails->searchForUpdate($update);
                if (!$email) {
                    $logger->error('Email not found', (array)$update);
                    continue;
                }

                $email->setStatus($status = $update->getStatus());
                $logger->debug('Status updated for email', ['email' => $email->getId(), 'status' => (array)$status]);

                if ($email->isRejected()) {
                    $blacklist->addEmail(new BlacklistedEmail($email));
                }
            }

            $em->flush();

        } catch (AmazonSNSMessageException $e) {
            $logger->error($e->getMessage());
        }

        return new Response('OK');
    }
}
